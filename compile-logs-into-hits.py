#!/usr/bin/env python3

import apache_log_parser
import fdroid_metrics
import glob
import gzip
import json
import os
import re
import urllib.parse
from datetime import datetime, timezone


def add_to_hit_bucket(d, language, country):
    """add one log line to all the tallies"""
    if 'hits' not in d:
        d['hits'] = 0
    d['hits'] += 1

    if not country:
        country = '?'
    if 'hitsPerCountry' not in d:
        d['hitsPerCountry'] = dict()
    if country not in d['hitsPerCountry']:
        d['hitsPerCountry'][country] = 0
    d['hitsPerCountry'][country] += 1

    language = sanitize_language(language)
    if 'hitsPerLanguage' not in d:
        d['hitsPerLanguage'] = dict()
    if language not in d['hitsPerLanguage']:
        d['hitsPerLanguage'][language] = 0
    d['hitsPerLanguage'][language] += 1

    return d


def anonymize_hit_bucket(d):
    delete_countries = []
    hitsPerCountry = d.get('hitsPerCountry', {})
    for country, hits in hitsPerCountry.items():
        if hits < HIT_MIN:
            delete_countries.append(country)
    for country in delete_countries:
        del hitsPerCountry[country]
    if not hitsPerCountry or len(hitsPerCountry) < 2:
        if 'hitsPerCountry' in d:
            del d['hitsPerCountry']

    delete_languages = []
    hitsPerLanguage = d.get('hitsPerLanguage', {})
    for language, hits in hitsPerLanguage.items():
        if hits < HIT_MIN:
            delete_languages.append(language)
    for language in delete_languages:
        del hitsPerLanguage[language]
    if not hitsPerLanguage or len(hitsPerLanguage) < 2:
        if 'hitsPerLanguage' in d:
            del d['hitsPerLanguage']


def remove_unanonymous_hit_buckets(d):
    delete_items = []
    for item, bucket in d.items():
        anonymize_hit_bucket(bucket)
        if bucket['hits'] < HIT_MIN:
            delete_items.append(item)
    for item in delete_items:
        del d[item]


def sanitize_language(language):
    if not language or not re.match(r'^[a-z]{2,3}([_-][A-Za-z0-9]{2,})*$', language):
        return '?'
    return language


HIT_MIN = 100
TOR = '''0.0.0.0 - %u %t "%r" %>s %b "%{Referer}i" "-" %{GEOIP_COUNTRY_CODE}e'''


def main(logdir, domain):

    git_repo = fdroid_metrics.get_metrics_data_repo(domain)
    fdroid_metrics.pull_commits(git_repo)
    hit_buckets = dict()

    for f in sorted(glob.glob(fdroid_metrics.get_hit_bucket_file_name(domain, fdroid_metrics.DATA_JSON_GLOB))):
        print('Loading', f)
        data = fdroid_metrics.load_hit_bucket_json(f)
        hit_buckets[data['weekStartDate']] = data

    did_show_start = False
    for f in sorted(
        glob.glob(os.path.join(logdir, 'access.log.*.gz')),
        key=lambda path: int(path.split('.')[-2]),
        reverse=True,
    ):
        print('Processing', f, flush=True)
        line_parser = apache_log_parser.make_parser(apache_log_parser.APACHE_COMBINED)
        with gzip.open(f, 'rt') as fp:
            for line in fp:
                try:
                    parsed = line_parser(line)
                except apache_log_parser.LineDoesntMatchException:
                    print('Switching Apache LogFormat from "combined" to "privacy"')
                    line_parser = apache_log_parser.make_parser(TOR)
                    parsed = line_parser(line)
                line_date = parsed['time_received_utc_datetimeobj']
                url = urllib.parse.urlparse(parsed['request_url'])
                country = parsed.get('env_geoip_country_code', '-')
                weekStartDate = fdroid_metrics.get_week_start_date(line_date)
                if weekStartDate not in hit_buckets:
                    hit_buckets[weekStartDate] = {
                        'errors': {},
                        'firstProcessedDate': line_date,
                        'paths': {},
                        'queries': {},
                        'weekStartDate': weekStartDate,
                    }
                hit_bucket = hit_buckets[weekStartDate]

                # Even though the logic should be <=, there isn't
                # enough time resolution to support that.  If there
                # are many log lines with the same time, then using <=
                # will only register the first one of those lines.
                if line_date < hit_bucket.get(
                    'lastProcessedDate',
                    datetime.fromtimestamp(0).replace(tzinfo=timezone.utc),
                ):
                    continue
                if not did_show_start:
                    print('Starting processing at:')
                    print(line)
                    did_show_start = True
                hit_bucket['lastProcessedDate'] = line_date

                language = None
                if url.query:
                    qs = urllib.parse.parse_qs(url.query)
                    q = qs.get('q', [''])[-1].lower().strip()
                    language = qs.get('lang', ['-'])[-1]
                    if q not in hit_bucket['queries']:
                        hit_bucket['queries'][q] = dict()
                    query = hit_bucket['queries'][q]
                    add_to_hit_bucket(query, language, country)
                add_to_hit_bucket(hit_bucket, language, country)

                status = parsed['status']
                if int(status) >= 400:
                    if status not in hit_bucket['errors']:
                        hit_bucket['errors'][status] = {'hits': 0, 'paths': {}}
                    hit_bucket['errors'][status]['hits'] += 1
                    if url.path not in hit_bucket['errors'][status]['paths']:
                        hit_bucket['errors'][status]['paths'][url.path] = 0
                    hit_bucket['errors'][status]['paths'][url.path] += 1
                else:
                    if url.path not in hit_bucket['paths']:
                        hit_bucket['paths'][url.path] = dict()
                    path = hit_bucket['paths'][url.path]
                    add_to_hit_bucket(path, language, country)

    # remove all entries with very low hit counts for private and usefulness
    for hit_bucket in hit_buckets.values():
        delete_errors = []
        for error, bucket in hit_bucket['errors'].items():
            if bucket['hits'] < HIT_MIN:
                delete_errors.append(error)
            delete_paths = set()
            for path, hits in bucket['paths'].items():
                if hits < HIT_MIN:
                    delete_paths.add(path)
            for path in delete_paths:
                del bucket['paths'][path]
        for error in delete_errors:
            del hit_bucket['errors'][error]
        anonymize_hit_bucket(hit_bucket)
        remove_unanonymous_hit_buckets(hit_bucket['paths'])
        remove_unanonymous_hit_buckets(hit_bucket['queries'])

    for weekStartDate, hit_bucket in hit_buckets.items():
        date = weekStartDate.strftime('%Y-%m-%d')
        with open(fdroid_metrics.get_hit_bucket_file_name(domain, date), 'w') as fp:
            json.dump(
                hit_bucket,
                fp,
                cls=fdroid_metrics.Encoder,
                indent=2,
                sort_keys=True,
                ensure_ascii=False,
            )

    fdroid_metrics.commit_compiled_json(git_repo)
    fdroid_metrics.push_commits(git_repo)


if __name__ == "__main__":
    import sys

    main(sys.argv[1], sys.argv[2])
