#!/bin/bash
#
# Simple pre-commit hook to check that there are no errors in the fdroidserver
# source files.

# Redirect output to stderr.
exec 1>&2

files=`git diff-index --cached HEAD 2>&1 | sed 's/^:.*     //' | uniq | cut -b100-500`
if [ -z "$files" ]; then
    BASH_FILES="hooks/install-hooks.sh hooks/pre-commit"
    JSON_FILES="*/*.json"
    PY_FILES="*.py"
    YML_FILES=".*.yml"
else
    # if actually committing right now, then only run on the files
    # that are going to be committed at this moment
    BASH_FILES=
    JSON_FILES=
    PY_FILES=
    YML_FILES=

    for f in $files; do
        test -e $f || continue
        case $f in
            *.json)
                JSON_FILES+=" $f"
                ;;
            *.py)
                PY_FILES+=" $f"
                ;;
            *.yml)
                YML_FILES+=" $f"
                ;;
            *)
                if head -1 $f | grep '^#!/bin/sh' > /dev/null 2>&1; then
                    BASH_FILES+=" $f"
                elif head -1 $f | grep '^#!/bin/bash' > /dev/null 2>&1; then
                    BASH_FILES+=" $f"
                elif head -1 $f | grep '^#!.*python' > /dev/null 2>&1; then
                    PY_FILES+=" $f"
                fi
                ;;
        esac
    done
fi

# We ignore the following PEP8 warnings
# * E501: line too long (82 > 79 characters)
#   - Recommended for readability but not enforced
#   - Some lines are awkward to wrap around a char limit

PEP8_IGNORE="E501"

err() {
	echo >&2 ERROR: "$@"
	exit 1
}

warn() {
	echo >&2 WARNING: "$@"
}

cmd_exists() {
	command -v $1 1>/dev/null
}

find_command() {
	for name in $@; do
		for suff in "3" "-3" "-python3" ""; do
			cmd=${name}${suff}
			if cmd_exists $cmd; then
				echo $cmd
				return 0
			fi
		done
	done
	warn "$1 is not installed, using dummy placeholder!"
	echo :
}

BASH=$(find_command bash)
JSONLINT=$(find_command jsonlint)
PEP8=$(find_command pycodestyle pep8)
PYFLAKES=$(find_command pyflakes)
YAMLLINT=$(find_command yamllint)

for f in $JSON_FILES; do
	if ! $JSONLINT $f 1>/dev/null; then
		err "$JSONLINT tests failed on $f!"
	fi
done

if [ "$PY_FILES" != " " ]; then
    if ! $PYFLAKES $PY_FILES; then
	err "pyflakes tests failed!"
    fi
    if ! $PEP8 --ignore=$PEP8_IGNORE $PY_FILES; then
	err "pep8 tests failed!"
    fi
fi

for f in $BASH_FILES; do
	if ! $BASH -n $f; then
		err "bash tests failed!"
	fi
done

for f in $YML_FILES; do
	if ! $YAMLLINT $f 1>/dev/null; then
		err ".yml tests failed on $f!"
	fi
done


exit 0
